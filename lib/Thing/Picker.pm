package Thing::Picker {

    use Exporter qw(import);

	our @EXPORT_OK = qw(get_two_things);

	my @things = qw( cat dog bird giraffe gorilla mouse );

    sub get_two_things {
        my @i = (0 .. $#things);
        my $lhs = splice @i, rand(@i), 1;
        my $rhs = splice @i, rand(@i), 1;
        return [ map $things[$_], $lhs, $rhs ];
    }
}

__PACKAGE__
__END__

