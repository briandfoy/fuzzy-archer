use strict;
use warnings;

use ExtUtils::MakeMaker;

use lib qw(lib);

WriteMakefile(
  VERSION   => '0.01',
  PREREQ_PM => {
	'Mojolicious'           => '3.84',
	'JSON::XS'              => '0',
	'Data::GUID'            => '0',
	'Config::IniFiles'       => '0',
	'DateTime' => '0',
	'DateTime::Format::HTTP' => '0',
	'DateTime::Format::Strptime' => '0',
	'DateTime::TimeZone' => '0',
	'DateTime::TimeZone::Local' => '0',
	'Digest::SHA' => '0',
	'HTTP::Request' => '0',
	'JSON' => '0',
	'LWP::ConnCache' => '0',
	'LWP::Protocol::https' => '0',
	'LWP::UserAgent' => '0',
	'Moose' => '0',
	'Net::Amazon::AWSSign' => '0',
	'Time::HiRes' => '0',
	'XML::Simple' => '0',
},
  test      => {TESTS => 't/*.t'}
);
