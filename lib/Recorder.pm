use v5.14;

package Recorder {
 	use Config::IniFiles;
	use Exporter qw( import );
	use File::Spec::Functions qw( catfile );
	use JSON::XS qw( encode_json );
	use Net::Amazon::DynamoDB;

	our @EXPORT_OK = qw( record_answer record_question );

	my $config = Config::IniFiles->new(
		-file => ( $ENV{AWS_FILE} // catfile( $ENV{HOME}, '.aws' ) )
		);

	my $question_table_name = $config->val( 'FuzzyArcher', 'question_table_name' );
	my $answer_table_name   = $config->val( 'FuzzyArcher', 'answer_table_name' );

	my $tables = {
		$question_table_name => {
			hash_key   => 'guid',
			attributes => {
				guid       => 'S',
				ip_address => 'S',
				'time'     => 'S',
				things     => 'S', # serialize
				},
			},
		$answer_table_name => {
			hash_key => 'guid',
			attributes => {
				guid       => 'S',
				ip_address => 'S',
				'time'     => 'S',
				side       => 'S',
				},
			},
		};

	my $ddb = Net::Amazon::DynamoDB->new(
        access_key => $config->val( 'AWS', 'access_key_id' ),
        secret_key => $config->val( 'AWS', 'secret_access_key' ),
  		host       => $config->val( 'FuzzyArcher', 'host' ),
        tables     => $tables,
		);

	foreach my $table ( keys $tables ) {
		$ddb->create_table( $table, 1, 1 )
			unless $ddb->exists_table( $table );
		}

	sub record_answer {
		my( $guid, $side, $ip_address ) = @_;

		# has time expired?
		# get time from same guid in question table

		# has question been answered already?
		$ddb->put_item( $answer_table_name => {
			guid       => $guid,
			ip_address => $ip_address,
			'time'     => time,
			side       => $side,
			} );
		}

	sub record_question {
		my( $guid, $things, $ip_address ) = @_;

		# serialize things
		my $serialized = encode_json( $things );
		$ddb->put_item( $question_table_name => {
			guid       => $guid,
			ip_address => $ip_address,
			'time'     => time,
			things     => $serialized,
			} );
		}
	}

__PACKAGE__
__END__
